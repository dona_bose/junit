package base;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class WebTestBase {
	protected static WebDriver driver;
	protected static String baseUrl="https://qtm4j404releaseprod.atlassian.net";
	protected static boolean acceptNextAlert = true;
	protected static StringBuffer verificationErrors = new StringBuffer();
	protected InputStream inputStream;

	@Before
	public void setUp() throws Exception {
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setHeadless(System.getenv("qasHeadlessMode")!=null?(System.getenv("qasHeadlessMode").equals("true")?true:false):false);
		driver=new ChromeDriver(chromeOptions);
		baseUrl = "";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
